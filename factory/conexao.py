import sqlite3
class ConexaoFactoryPattern(object):
    def __init__(self):
        self.conexao = sqlite3.connect('elegebd.db')
        self.criarTabelas()

    def getConexao(self):
        return self.conexao

    def criarTabelas(self):
        self.conexao.execute('''CREATE TABLE IF NOT EXISTS TANQUE
        (COD INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        CAPACIDADE INTEGER,
        TIPO TEXT NOT NULL,
        CARGA INTEGER)''')

        self.conexao.execute('''CREATE TABLE IF NOT EXISTS USUARIO
        (COD INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        LOGIN TEXT NOT NULL,
        SENHA TEXT NOT NULL)''')

    def inicializarDados(self):
        self.criarTabelas()

        sql"""SELECT * FROM USUARIO"""
        self.cursor.execute(sql)
        usuarios = self.cursor.fetchall()

        if usuario == []:
            sql = """INSERT INTO USUARIO (LOGIN,SENHA) VALUES("admin","admin")"""
