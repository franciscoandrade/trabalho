from abc import abstractmethod,ABC
class TanqueDaoPattern(object):

    @abstractmethod
    def saveOrUpdate(self, tanque):
        pass

    @abstractmethod
    def getTanque(self):
        pass

    @abstractmethod
    def getLista(self):
        pass

    @abstractmethod
    def getListaPesquisa(self, parametro):
        pass

    @abstractmethod
    def excluir(self, parametro):
        pass

    @abstractmethod
    def setCarga(self, parametro, carga):
        pass

    @abstractmethod
    def getCarga(self, parametro, carga):
        pass
