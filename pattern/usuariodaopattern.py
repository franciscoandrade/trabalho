from abc import abstractmethod,ABC
class UsuarioDaoPattern(object):

    @abstractmethod
    def saveOrUpdate (self, usuario):
        pass

    @abstractmethod
    def getLista(self):
        pass

    @abstractmethod
    def getListaPesquisa(self,parametro):
        pass

    @abstractmethod
    def excluir(self, parametro):
        pass
