from models.tanque  import Tanque
from pattern.tanquedaopattern import *

class TanqueDao(object):

    def __init__(self, conexao):
        self.conexao = conexao

    def saveOrUpdate (self, tanque):
        sql = ""
        if (tanque.cod == 0):
            sql = 'insert into tanque (capacidade,tipo) values (?,?)'
            self.conexao.execute(sql,(tanque.capacidade, tanque.tipo))
        else:
            sql = 'update tanque set capacidade = ? ,tipo = ? where cod = ?'
            self.conexao.execute(sql,(tanque.capacidade, tanque.tipo,tanque.cod))
        self.conexao.commit()

    def getTanque(self):
        sql = "select * from tanque where cod = ?"
        resultado = self.conexao.execute(sql, (parametro,))
        for valor in resultado:
            tanque = Tanque(valor[0],valor[1],valor[2],valor[3])
            return tanque

    def getLista(self):
        sql = "select * from tanque"
        resultado = self.conexao.execute(sql)
        lista = []
        for valor in resultado:
            tanque = Tanque(valor[0],valor[1],valor[2],valor[3])
            lista.append(tanque)
        return lista

    def getListaPesquisa(self,parametro):
        if parametro == 1:
            sql = "select * from tanque where cod = ?"
            resultado = self.conexao.execute(sql, (parametro,))
            lista = []
            for valor in resultado:
                tanque = Tanque(valor[0],valor[1],valor[2],valor[3])
                lista.append(tanque)
            return lista
        if parametro == 2:
            sql = "select * from tanque where capacidade = ?"
            resultado = self.conexao.execute(sql, (parametro,))
            lista = []
            for valor in resultado:
                tanque = Tanque(valor[0],valor[1],valor[2],valor[3])
                lista.append(tanque)
            return lista
        if parametro == 3:
            sql = "select * from tanque where tipo = ?"
            resultado = self.conexao.execute(sql, (parametro,))
            lista = []
            for valor in resultado:
                tanque = Tanque(valor[0],valor[1],valor[2],valor[3])
                lista.append(tanque)
            return lista
        if parametro == 4:
            sql = "select * from tanque where carga = ?"
            resultado = self.conexao.execute(sql, (parametro,))
            lista = []
            for valor in resultado:
                tanque = Tanque(valor[0],valor[1],valor[2],valor[3])
                lista.append(tanque)
            return lista

    def excluir(self, parametro):
        sql = "delete from tanque where cod = ?"
        self.conexao.execute(sql, (parametro,))
        self.conexao.commit()

    def setCarga(self, parametro, carga):
        pass



#Ver se esta certo-----------------------------
    def getCarga(self, parametro):
        sql = "select * from tanque where carga = ?"
        resultado = self.conexao.execute(sql, (parametro,))
        lista = []
        for valor in resultado:
            tanque = Tanque(valor[0],valor[3])
            lista.append(tanque)
        return lista
