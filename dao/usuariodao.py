from models.usuario  import Usuario
from pattern.usuariodaopattern import *

class UsuarioDao(object):

    def __init__(self, conexao):
        self.conexao = conexao

    def saveOrUpdate (self, usuario):
        sql = ""
        if (usuario.cod == 0):
            sql = 'insert into usuario (login,senha) values (?,?)'
            self.conexao.execute(sql,(usuario.login, usuario.senha))
        else:
            sql = 'update usuario set login = ? ,senha = ? where cod = ?'
            self.conexao.execute(sql,(usuario.login, usuario.senha,usuario.cod))
        self.conexao.commit()

    def getLista(self):
        sql = "select * from usuario"
        resultado = self.conexao.execute(sql)
        lista = []
        for valor in resultado:
            usuario = Usuario(valor[0],valor[1],valor[2])
            lista.append(usuario)
        return lista

    def getListaPesquisa(self,parametro):
        sql = "select * from usuario where cod = ?"
        resultado = self.conexao.execute(sql, (parametro,))
        for valor in resultado:
            usuario = Usuario(valor[0],valor[1],valor[2])
            return usuario

    def excluir(self, parametro):
        sql = "delete from usuario where cod = ?"
        self.conexao.execute(sql, (parametro,))
        self.conexao.commit()
